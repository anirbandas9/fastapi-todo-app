from fastapi import FastAPI, HTTPException, Depends
from typing import List
from models import TodoItem, User
import json

data = json.load(open("data.json"))
user = data["users"]
todos = data["todos"]

app = FastAPI()


# Implement a function to get the current user
@app.get("/api/v1/users/me", response_model=User, tags=["users"])
async def get_current_user(user_id: int = 1):
    for u in user:
        if u["id"] == user_id:
            return u
    raise HTTPException(status_code=404, detail="User not found")


# Endpoint to create a new user
@app.post("/api/v1/users", tags=["users"])
async def create_new_user(name: str) -> dict:
    new_user = {
        "id": user[-1]["id"] + 1,
        "name": name
    }
    user.append(new_user)
    return {"data": f"User {name} added successfully"}


# Endpoint to update a user
@app.put("/api/v1/users/{user_id}", tags=["users"])
async def update_user(user_id: int, name: str) -> dict:
    # Check if user exists
    user_exists = False
    user_index = None
    for index, u in enumerate(user):
        if u["id"] == user_id:
            user_exists = True
            user_index = index
            break
    if not user_exists:
        raise HTTPException(status_code=404, detail="User not found")

    # Update user
    user[user_index]["name"] = name
    return {"data": f"User {name} updated successfully"}


# Endpoint to delete a user
@app.delete("/api/v1/users/{user_id}", tags=["users"])
async def delete_user(user_id: int) -> dict:
    # Check if user exists
    user_exists = False
    user_index = None
    for index, u in enumerate(user):
        if u["id"] == user_id:
            user_exists = True
            user_index = index
            break
    if not user_exists:
        raise HTTPException(status_code=404, detail="User not found")

    # Delete user
    user.pop(user_index)
    return {"data": f"User with id {user_id} deleted successfully"}


@app.get("/", tags=["Root"])
async def root() -> dict:
    return {"Home": "Welcome to the Todo app"}


# Endpoint to get all todos
@app.get("/api/v1/todos", response_model=List[TodoItem], tags=["todos"])
async def get_all_todos():
    return todos


# Endpoint to get all todos for a user - anyone can view this
@app.get("/api/v1/todos/{user_id}", response_model=List[TodoItem], tags=["todos"])
async def get_user_todos(user_id: int):
    # Check if user exists
    user_exists = False
    for u in user:
        if u["id"] == user_id:
            user_exists = True
            break
    if not user_exists:
        raise HTTPException(status_code=404, detail="User not found")

    # Get todos for the user
    user_todos = []
    for todo in todos:
        if todo["user_id"] == user_id:
            user_todos.append(todo)
    return user_todos


# Endpoint to create a new todo
@app.post("/api/v1/todos", tags=["todos"])
async def create_new_todo(task: str, current_user: User = Depends(get_current_user)) -> dict:
    new_todo = {
        "id": todos[-1]["id"] + 1,
        "user_id": current_user["id"],
        "task": task
    }
    todos.append(new_todo)
    return {"data": f"Todo added successfully for user {current_user['name']}"}


# Endpoint to update a todo
@app.put("/api/v1/todos/{todo_id}", response_model=TodoItem, tags=["todos"])
async def update_todo(todo_id: int, task: str, current_user: User = Depends(get_current_user)):
    # Check if todo exists
    todo_exists = False
    todo_index = None
    for index, todo in enumerate(todos):
        if todo["id"] == todo_id:
            todo_exists = True
            todo_index = index
            break
    if not todo_exists:
        raise HTTPException(status_code=404, detail="Todo not found")

    # Check if todo belongs to current user
    if todos[todo_index]["user_id"] != current_user["id"]:
        raise HTTPException(status_code=401, detail="You are not authorized to update this todo")

    # Update the todo
    todos[todo_index]["task"] = task
    return todos[todo_index]


# Endpoint to delete a todo
@app.delete("/api/v1/todos/{todo_id}", response_model=TodoItem, tags=["todos"])
async def delete_todo(todo_id: int, current_user: User = Depends(get_current_user)):
    # Check if todo exists
    todo_exists = False
    todo_index = None
    for index, todo in enumerate(todos):
        if todo["id"] == todo_id:
            todo_exists = True
            todo_index = index
            break
    if not todo_exists:
        raise HTTPException(status_code=404, detail="Todo not found")

    # Check if todo belongs to current user
    if todos[todo_index]["user_id"] != current_user["id"]:
        raise HTTPException(status_code=401, detail="You are not authorized to delete this todo")

    # Delete the todo
    deleted_todo = todos[todo_index]
    del todos[todo_index]
    return deleted_todo


# # Placeholder data for users and todos
# user = [
#     {
#         "id": 1,
#         "name": "John Doe"
#     },
#     {
#         "id": 2,
#         "name": "Honey Doe"
#     }
# ]

# todos = [
#     {
#         "id": 1,
#         "user_id": 1,
#         "task": "Buy groceries"
#     },
#     {
#         "id": 2,
#         "user_id": 2,
#         "task": "Clean the house"
#     },
# ]

# # Get all Todos
# @app.get("/api/v1/todos", tags=["todos"])
# async def get_all_todos() -> dict:
#     return {"data": todos}


# # Get todo my id
# @app.get("/api/v1/todos/{todo_id}", tags=["todos"])
# async def get_todo(todo_id: int) -> dict:
#     for todo in todos:
#         if int(todo["id"]) == todo_id:
#             return {"data": todo}
#     raise HTTPException(status_code=404, detail="Todo not found")


# # Create a new todo
# @app.post("/api/v1/todos", tags=["todos"])
# async def create_todo(todo: dict) -> dict:
#     todos.append(todo)
#     return {"data": "A new todo has been added."}


# # Update an existing Todo
# @app.put("/api/v1/todos/{todo_id}", tags=["todos"])
# async def update_todo(todo_id: int, data: dict) -> dict:
#     for todo in todos:
#         if int(todo["id"]) == todo_id:
#             todo["task"] = data["task"]
#             return {"data": f"Todo with id {todo_id} has been updated."}
#     raise HTTPException(status_code=404, detail=f"Todo with id {todo_id} not found.")


# # Delete an existing Todo
# @app.delete("/api/v1/todos/{todo_id}", tags=["todos"])
# async def delete_todo(todo_id: int) -> dict:
#     for todo in todos:
#         if int(todo["id"]) == todo_id:
#             todos.remove(todo)
#             return {"data": f"Todo with id {todo_id} was successfully deleted"}
#     raise HTTPException(status_code=404, detail=f"Todo with id {todo_id} not found.")
