from pydantic import BaseModel


# Model for TODOs
class TodoItem(BaseModel):
    id: int
    user_id: int
    task: str


# Model for Users
class User(BaseModel):
    id: int
    name: str
